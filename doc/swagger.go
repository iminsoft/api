package doc

import (
	"github.com/emicklei/go-restful"
	"github.com/emicklei/go-restful/swagger"
)

func RegisterSwagger(c *restful.Container, prefix string) {
	config := swagger.Config{
		WebServices: c.RegisteredWebServices(), // you control what services are visible
		//WebServicesUrl: "http://localhost:8888",
		ApiPath: "/" + prefix + "/apidocs.json",
		// Optionally, specifiy where the UI is located
		SwaggerPath:     "/" + prefix + "/apidocs/",
		SwaggerFilePath: "swagger-ui/dist/"}

	swagger.RegisterSwaggerService(config, c)

}
