package api

import (
	"github.com/micro/go-micro/client"
	"github.com/mikespook/golib/log"
	"golang.org/x/net/context"
)

type (
	Fetcher interface {
		Fetch(srv, name string, req, res interface{}) error
	}

	jsonFetcher struct {
		c client.Client
	}
)

func (f *jsonFetcher) Fetch(srv, name string, req, res interface{}) error {
	err := f.c.Call(context.Background(), f.c.NewRequest(srv, name, req), res)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func NewFetcher() Fetcher {
	return &jsonFetcher{
		c: client.NewClient(
			client.ContentType("application/json"),
		),
	}
}
