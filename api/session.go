package api

import (
	"bitbucket.org/iminsoft/auth-srv/model"
	"github.com/emicklei/go-restful"
	"net/http"
)

const (
	sessionServer = "im.srv.auth"
)

type (
	Session struct {
		Fetcher
	}
)

func (s *Session) Create(in *restful.Request, out *restful.Response) {
	req := &model.LoginRequest{}
	res := &model.LoginResponse{}

	in.ReadEntity(req)

	if err := s.Fetch(sessionServer, "Auth.Login", req, res); err != nil {
		out.WriteError(http.StatusUnauthorized, err)
		return
	}

	if res.Session == nil {
		out.WriteErrorString(http.StatusUnauthorized, "Wrong username/password or account do not exists")
		return
	}

	out.WriteEntity(res.Session)

}

func (s *Session) Delete(in *restful.Request, out *restful.Response) {
	req := &model.LogoutRequest{
		SessionId: in.PathParameter("session-id"),
	}
	res := &model.LogoutResponse{}

	if err := s.Fetch(sessionServer, "Auth.Logout", req, res); err != nil {
		out.WriteErrorString(http.StatusInternalServerError, "Could not connect to session server")
		return
	}
}

func (s *Session) Read(in *restful.Request, out *restful.Response) {
	req := &model.SessionRequest{
		SessionId: in.PathParameter("session-id"),
	}
	res := &model.SessionResponse{}

	if err := s.Fetch(sessionServer, "Auth.Session", req, res); err != nil {
		out.WriteErrorString(http.StatusInternalServerError, "Could not connect to session server")
		return
	}

	out.WriteEntity(res.Session)

}

func (l *Session) Register(container *restful.Container) {
	ws := new(restful.WebService).
		Path("/api/session").
		Doc("Employees session managment").
		Consumes(restful.MIME_XML, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_XML)

	ws.Route(ws.PUT("/").To(l.Create).
		Doc("Create session").
		Reads(model.LoginRequest{}).
		Writes(model.Session{}))

	ws.Route(ws.DELETE("/{session-id}").To(l.Delete).
		Doc("Destroy session").
		Param(ws.PathParameter("session-id", "session identifier").DataType("string")))

	ws.Route(ws.GET("/{session-id}").To(l.Read).
		Doc("Read session").
		Param(ws.PathParameter("session-id", "session identifier").DataType("string")).
		Writes(model.Session{}))

	container.Add(ws)
}

func RegisterSession(c *restful.Container, f Fetcher) {
	e := &Session{
		f,
	}
	e.Register(c)

}
