package api

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	employee "bitbucket.org/iminsoft/employee-srv/model"
	rbac "bitbucket.org/iminsoft/rbac/model"
	vlan "bitbucket.org/iminsoft/vlan-srv/handler"
	api "github.com/emicklei/go-restful"
)

const (
	employeeServer = "im.srv.employee"
	rbacServer     = "im.srv.rbac"
	vlanServer     = "im.srv.vlan"
)

var (
	robot []byte
)

type (
	Employees struct {
		Fetcher
	}
)

func (e *Employees) Search(in *api.Request, out *api.Response) {
	sl, _ := strconv.Atoi(in.QueryParameter("limit"))
	so, _ := strconv.Atoi(in.QueryParameter("offset"))

	s := &employee.SearchRequest{
		Text:   in.QueryParameter("text"),
		Limit:  int64(sl),
		Offset: int64(so),
		Sort:   in.QueryParameter("sort"),
		Order:  in.QueryParameter("order"),
	}
	r := &employee.SearchResponse{}

	if err := e.Fetch(employeeServer, "Employee.Search", s, r); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}

	out.WriteEntity(r)

}

func (e *Employees) Get(in *api.Request, out *api.Response) {
	req := &employee.GetRequest{
		Gid: in.PathParameter("id"),
	}

	res := &employee.GetResponse{}

	if err := e.Fetch(employeeServer, "Employee.Get", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}

	if res.Employee.Gid == "" {
		out.WriteErrorString(http.StatusNotFound, "Employee not found")
		return
	}

	out.WriteEntity(res.Employee)
}

func (e *Employees) Modify(in *api.Request, out *api.Response) {
	emp := &employee.Employee{}
	req := &employee.ModifyRequest{}
	res := &employee.ModifyResponse{}

	if err := in.ReadEntity(emp); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Wrong format of employee")
		return
	}

	req.Employee = *emp
	req.Employee.Gid = in.PathParameter("id")
	if err := e.Fetch(employeeServer, "Employee.Modify", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Could not connect to employees server")
		return
	}

	greq := &employee.GetRequest{
		Gid: req.Employee.Gid,
	}

	gres := &employee.GetResponse{}
	if err := e.Fetch(employeeServer, "Employee.Get", greq, gres); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Could not connect to employees server")
		return
	}

	out.WriteEntity(gres.Employee)

}

func (e *Employees) Avatar(in *api.Request, out *api.Response) {
	out.AddHeader("Content-Type", "image")
	req := &employee.AvatarRequest{
		Gid: in.PathParameter("id"),
	}

	res := &employee.AvatarResponse{}

	if err := e.Fetch(employeeServer, "Employee.Avatar", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Could not connect to employees server")
		return
	}
	var s []byte
	s, err := base64.StdEncoding.DecodeString(res.Source)
	if err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Image file corupted")
		return
	}

	if len(s) == 0 {
		s = randomAvatar(req.Gid)
	}
	out.Write(s)
}

func (e *Employees) AvatarCreate(in *api.Request, out *api.Response) {
	req := &employee.AvatarUploadRequest{
		Gid: in.PathParameter("id"),
	}
	if err := in.ReadEntity(req); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Wrong format of employee")
		return
	}

	if err := e.Fetch(employeeServer, "Employee.AvatarUpload", req, nil); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}
}

func (e *Employees) GetPublicKey(in *api.Request, out *api.Response) {
	req := &employee.PublicKeyRequest{
		Gid: in.PathParameter("id"),
	}

	res := &employee.PublicKeyResponse{}

	if err := e.Fetch(employeeServer, "Employee.PublicKey", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}

	out.WriteEntity(res)
}

func (e *Employees) PutPublicKey(in *api.Request, out *api.Response) {
	req := &employee.ChangePublicKeyRequest{
		Gid: in.PathParameter("id"),
	}

	if err := in.ReadEntity(req); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Wrong format of employee")
		return
	}

	if err := e.Fetch(employeeServer, "Employee.ChangePublicKey", req, nil); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}
}

func (e *Employees) Roles(in *api.Request, out *api.Response) {
	req := &rbac.RolesRequest{
		Gid: in.PathParameter("id"),
	}
	res := &rbac.RolesResponse{}

	if err := e.Fetch(rbacServer, "Rbac.Roles", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}

	out.WriteEntity(res)
}

func (e *Employees) RoleIn(in *api.Request, out *api.Response) {
	req := &rbac.InRoleRequest{
		Gid:  in.PathParameter("id"),
		Role: in.PathParameter("name"),
	}

	res := &rbac.InRoleResponse{}

	if err := e.Fetch(rbacServer, "Rbac.InRole", req, res); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}

	out.WriteEntity(res)
}

func (e *Employees) VLan(in *api.Request, out *api.Response) {
	req := &vlan.GetRequest{
		Gid: in.PathParameter("id"),
	}

	res := &vlan.GetResponse{}
	if err := e.Fetch(vlanServer, "Vlan.Get", req, res); err != nil {
		out.WriteErrorString(http.StatusNotFound, err.Error())
		return
	}

	out.WriteEntity(res)

}

func (e *Employees) VLanUse(in *api.Request, out *api.Response) {
	req := &vlan.UseRequest{
		Gid: in.PathParameter("id"),
	}

	if err := in.ReadEntity(req); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Wrong request format")
		return
	}

	if err := e.Fetch(vlanServer, "Vlan.Use", req, nil); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}
}

func (e *Employees) VLanDefine(in *api.Request, out *api.Response) {
	req := &vlan.DefineRequest{
		Gid: in.PathParameter("id"),
	}

	if err := in.ReadEntity(req); err != nil {
		out.WriteErrorString(http.StatusBadRequest, "Wrong request format")
		return
	}

	if err := e.Fetch(vlanServer, "Vlan.Define", req, nil); err != nil {
		out.WriteErrorString(http.StatusBadRequest, err.Error())
		return
	}
}

func (e *Employees) Register(container *api.Container) {
	ws := new(api.WebService).
		Path("/api/employees").
		Doc("NewCo Employees index").
		Consumes(api.MIME_XML, api.MIME_JSON).
		Produces(api.MIME_JSON, api.MIME_XML)

	ws.Route(ws.GET("/").To(e.Search).
		Doc("Search employees").
		Param(ws.QueryParameter("text", "one of personal data attributes ie. address, department, name").DataType("string")).
		Param(ws.QueryParameter("sort", "field used for sorting, ie: username, created").DataType("string")).
		Param(ws.QueryParameter("order", "manage order of results: asc or desc").DataType("string")).
		Param(ws.QueryParameter("offset", "").DataType("int")).
		Param(ws.QueryParameter("limit", "").DataType("int")).
		Writes(employee.SearchResponse{}))

	ws.Route(ws.GET("/{id}").To(e.Get).
		Doc("Gets particular employee, identified by it's GID").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Writes(employee.Employee{}))

	ws.Route(ws.POST("/{id}").To(e.Modify).
		Doc("Modify employee record").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Reads(employee.Employee{}).
		Writes(employee.Employee{}))

	ws.Route(ws.GET("/{id}/avatar").To(e.Avatar).
		Doc("Binary image file, photography of employee").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")))

	ws.Route(ws.PUT("/{id}/avatar").To(e.AvatarCreate).
		Doc("Create or Replace employee photography").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Reads(employee.AvatarUploadRequest{}))

	ws.Route(ws.GET("/{id}/public-key").To(e.GetPublicKey).
		Doc("Get RSA public key of employee").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")))

	ws.Route(ws.PUT("/{id}/public-key").To(e.PutPublicKey).
		Doc("Create or Replace RSA public key of employee").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Reads(employee.ChangePublicKeyRequest{}))

	ws.Route(ws.GET("/{id}/roles").To(e.Roles).
		Doc("List of roles").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Writes(rbac.RolesResponse{}))

	ws.Route(ws.GET("/{id}/roles/{name}").To(e.RoleIn).
		Doc("Check if employee has given role").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Param(ws.PathParameter("name", "one of names").DataType("string")).
		Writes(rbac.InRoleResponse{}))

	ws.Route(ws.GET("/{id}/vlan/").To(e.VLan).
		Doc("Show list of available VLAN's and currently used.").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Writes(vlan.GetResponse{}))

	ws.Route(ws.POST("/{id}/vlan").To(e.VLanUse).
		Doc("Change current VLAN").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Reads(vlan.UseRequest{}))

	ws.Route(ws.PUT("/{id}/vlan").To(e.VLanDefine).
		Doc("Change list of available VLAN's").
		Param(ws.PathParameter("id", "unique employee identifier").DataType("string")).
		Reads(vlan.DefineRequest{}))

	container.Add(ws)
}

func RegisterEmployees(c *api.Container, f Fetcher) {
	e := &Employees{
		f,
	}
	e.Register(c)

}
func randomAvatar(gid string) []byte {

	if robot != nil {
		return robot
	}

	res, err := http.Get("https://www.prosci.com/media/wysiwyg/icons/icon-frontlineemployees-dark.png")
	//res, err := http.Get("https://robohash.org/" + gid)
	if err != nil {
		log.Fatal(err)
	}
	robot, err = ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	return robot
}
