package main

import (
	"bitbucket.org/iminsoft/api/api"
	"bitbucket.org/iminsoft/api/doc"
	"github.com/emicklei/go-restful"
	"github.com/micro/go-web"
	"log"
)

const proxyName = "api"

func main() {

	container := restful.NewContainer()
	fetcher := api.NewFetcher()
	service := web.NewService(
		web.Name("im.api." + proxyName),
	)

	service.Init()

	api.RegisterEmployees(container, fetcher)
	api.RegisterSession(container, fetcher)
	doc.RegisterSwagger(container, proxyName)

	service.Handle("/", container)

	// Run server
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
